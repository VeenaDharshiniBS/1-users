/*const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}
*/

//Q1 Find all users who are interested in playing video games.
function intersetedInVideoGames(data)
{
    return Object.keys(data).reduce((acc,user)=>{
        if(data[user].interests[0].includes("Video Games"))
        {
            acc[user] = data[user];
        }
        return acc;
    },{});
}

//console.log(intersetedInVideoGames(users));

//Q2 Find all users staying in Germany.
function stayingInGermany(data)
{
    return Object.keys(data).reduce((acc,user)=>{
        if(data[user].nationality ==  "Germany")
        {
            acc[user] = data[user];
        }
        return acc;
    },{});
}

//console.log(stayingInGermany(users));

// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 1

function sortingBySeniority(data)
{
    data = Object.fromEntries(Object.entries(data).map((user)=>{
        if(user[1].desgination.includes("Senior") && user[1].desgination.includes("Developer"))
            user[1].position = 1;
        else if(user[1].desgination.includes("Developer"))
            user[1].position = 2;
        else if(user[1].desgination.includes("Intern"))
            user[1].position = 3;
        return user;
    }));
    
    return Object.fromEntries(
        Object.entries(data).sort(([,user1],[,user2]) => {
                if(user1.position>user2.position)
                    return 1;
                else if(user1.position<user2.position)
                    return -1;
                else
                {
                    if(user1.age<user2.age)
                        return 1;
                    else if(user1.age>user2.age)
                        return -1;
                    else
                        return 0;
                }
            })
    );
}

//console.log(sortingBySeniority(users));

// Q4 Find all users with masters Degree.
function masterDegree(data)
{
    let res = Object.keys(data).reduce((acc,user)=>{
        if(data[user].qualification ==  "Masters")
        {
            acc[user] = data[user];
        }
        return acc;
    },{});
    return res;
}

//console.log(masterDegree(users));

// Q5 Group users based on their Programming language mentioned in their designation.
function groupByProgrammingLanguage(data)
{
    let groupUsers = Object.keys(data).reduce((language,user)=>{
        if(data[user].desgination.includes('Golang'))
        {
            language.Golang[user] = JSON.stringify(data[user]);
        }
        else if(data[user].desgination.includes('Javascript'))
        {
            language.Javascript[user] = JSON.stringify(data[user]);
        }
        else if(data[user].desgination.includes('Python'))
        {
            language.Python[user] = JSON.stringify(data[user]);
        }
        return language;
    },{"Golang":{}, "Javascript":{}, "Python":{}});

    return groupUsers;
}

//console.log(groupByProgrammingLanguage(users));